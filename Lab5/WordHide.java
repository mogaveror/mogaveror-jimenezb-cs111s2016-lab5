
//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Brian Jimenez, Robert Mogavero
// CMPSC 111 Spring 2016
// Lab #5
// Date:02/17/2016
//
// Purpose: Write a program that hides 10 letter words and creates 20x20 grid
//*************************************
import java.util.Date; // needed for printing today's date

public class WordHide
{
	//----------------------------
	// main method: program execution begins here
	//----------------------------
		public static void main(String[] args)
		{
		// Label output with name and date:
		System.out.println("Brian Jimenez,Robert Mogavero, Lab # 5" + new Date());
		// Variable dictionary:
		String Instruction= "Input a word you wish to hide that is 10 characters in length:";//Gives instructions on length requirement
		String phrase = "fryingpans";		//Write Down Word
		String Mutation1= phrase.substring(0,10); // only 10 letter words are able to be inputted
		String Mutation2= Mutation1.toUpperCase();//Capitalize the word
		String letter1= Mutation2.substring(0,1); //Get first letter of the word 
		String letter2= Mutation2.substring(1,2); //Get second letter of the word 
		String letter3= Mutation2.substring(2,3); //Get third letter of the word 
		String letter4= Mutation2.substring(3,4); //Get fourth letter of the word 
		String letter5= Mutation2.substring(4,5); //Get fifth letter of the word 
		String letter6= Mutation2.substring(5,6); //Get sixth letter of the word 
		String letter7= Mutation2.substring(6,7); //Get seventh letter of the word 
		String letter8= Mutation2.substring(7,8); //Get eigth letter of the word 
		String letter9= Mutation2.substring(8,9); //Get ninth letter of the word 
		String letter10= Mutation2.substring(9,10); //Get tenth letter of the word 
		String end1= " K J J F O J H W G B T";	//Finishes 6th line of the 20x20 grid
		String end2= " J K D B A L K J S D";	//Finishes 7th line of the 20x20 grid
		String end3= " S A B L I U G A D ";	//Finishes 8th line of the 20x20 grid
		String end4= " T H L K A S D I";	//Finishes 9th line of the 20x20 grid
		String end5= " O P A J P O J";		//Finishes 10th line of the 20x20 grid
		String end6= " B A S M N A";		//Finishes 11th line of the 20x20 grid
		String end7= " Q W D I U";		//Finishes 12th line of the 20x20 grid
		String end8= " F P O Q";		//Finishes 13th line of the 20x20 grid
		String end9= " Y W N";			//Finishes 14th line of the 20x20 grid
		String end10= " Z H";			//Finishes 15th line of the 20x20 grid
		
		System.out.println("" +Instruction);
		System.out.println("Hidden Word:"+letter1 +letter2 +letter3 +letter4 +letter5 +letter6 +letter7 +letter8 +letter9 +letter10); //Says the hidden word
		System.out.println("S W U Y P A K F G P I O H A S D K J D A");			//first line of 20x20 grid
		System.out.println("P L I Q W O L Q L K N A S D O H A P O I");			//second line of 20x20 grid
		System.out.println("A Q E F H Q N R K L N Q W R O P I H F A");			//third line of 20x20 grid
		System.out.println("B A S D L K B J A D S O I H O U Q L N A");			//fourth line of the 20x20 grid
		System.out.println("D I P U Q W Q I U O G F A K N L A S D I");			//fifth line of 20x20 grid
		System.out.println("W F R O I P H Q " +letter1 +end1);				//sixth line of 20x20 grid
		System.out.println("S Q T A S D L A S " +letter2 +end2);			//seventh line of 20x20 grid
		System.out.println("A L I U W K M N S D " +letter3 +end3);			//eigth line of 20x20 grid
		System.out.println("B D A V Q E T O W I E " +letter4 +end4);			//ninth line of 20x20 grid
		System.out.println("H Q R V Q W S A G Y U I " +letter5 +end5);			//tenth line of 20x20 grid
		System.out.println("O A S H L Q I Y O Q R W B " +letter6 +end6);		//eleventh line of 20x20 grid
		System.out.println("K L B J A S K L B J D A L K " +letter7 +end7);		//twelveth line of 20x20 grid
		System.out.println("S A P B N Q I P U Q G U Q B W " +letter8 +end8);		//thirteenth line of 20x20 grid
		System.out.println("P Q O I W B F L A M S B V P L A " +letter9 +end9);		//fourteenth line of 20x20 grid
		System.out.println("P Q O W U E B A L S K J B V X C K " +letter10 +end10);	//fifthteenth line of 20x20 grid
		System.out.println("B A S D L K B J A D S O I H O U Q J L C");			//sixteenth line of 20x20 grid
		System.out.println("Q P O Q N K L A B K J S I U O W P I O Q"); 			//seventeenth line of 20x20 grid
		System.out.println("P Q U E K L B S F P A H F O Q F H Q F H");			//eighteenth line of 20x20 grid
		System.out.println("A Q E F H Q N R K L N Q W R O P I H F A");			//nineteenth line of 20x20 grid
		System.out.println("S W U Y P A K F G P I O H A S D K J D A");			//twentieth line of 20x20 grid
		}
	   }	
